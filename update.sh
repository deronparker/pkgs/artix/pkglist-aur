#!/bin/sh

echo "=================== UPDATING DEPENDENCIES ==================="
(
    cd ./dependencies
    for f in *; do
        if [[ -d "$f" ]]; then
            (
                cd "$f"
                makepkg -i --needed
            )
        fi
    done
)

echo "=================== UPDATING PKGS ==================="
for f in *; do
    if [[ -d "$f" ]]; then
        (
            cd "$f"
            if [[ "$f" = "dependencies" ]]; then
                echo "skipping dependencies"
            else
                makepkg -i --needed
            fi
        )
    fi
done
