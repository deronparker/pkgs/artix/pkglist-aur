#!/bin/sh

for f in *; do
    if [[ -d "$f" ]]; then
        (
            cd "$f"
            git clean -xf .
        )
    fi
done
