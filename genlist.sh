#!/bin/sh

cat /dev/null >./pkglist

for f in *; do
    (
        if [[ -d "$f" ]]; then
            if [[ "$f" != "dependencies" ]]; then
                cd "$f" || exit
                git remote get-url origin >>../pkglist
            fi
        fi
    )
done

(
    cd ./dependencies
    for f in *; do
        (
            if [[ -d "$f" ]]; then
                cd "$f" || exit
                url=$(git remote get-url origin)
                echo "dep:$url" >>../../pkglist
            fi
        )
    done
)
